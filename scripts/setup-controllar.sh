#!/bin/bash

set -e

# enable services
systemctl enable avahi-daemon
systemctl enable mosquitto
systemctl enable cron
systemctl enable docker
systemctl enable ssh
systemctl enable controllar-resize-image
systemctl enable openvpn
systemctl enable openvpn@client

# disabble getty on the serial that communicates with the main controller board
systemctl disable serial-getty@ttyS0.service
systemctl mask serial-getty@ttyS0.service

# enable firsts configuration services to orangepi and raspberrrypi
if [ $3 = "orangepi" ]; then
    systemctl enable orange-config
    rm /controllar/rasp-config.sh
    rm /etc/systemd/system/rasp-config.service
else
    systemctl enable rasp-config
    rm /controllar/orange-config.sh
    rm /etc/systemd/system/orange-config.service
fi

# Set East (Brasilia/SP) timezone
ln -sf /usr/share/zoneinfo/Brazil/East /etc/localtime

# ======================== Begin of services enable section ========================
# Images with Multimedia service
if [[ $2 == *"multimidia"* ]]; then
    systemctl enable broadlink-mqtt-update
fi
# Images with ML service
if [[ $2 == *"ml"* ]]; then
    systemctl enable api-server-controllar-ml-update
fi
# Images with Tasmota service
if [[ $2 == *"tasmota"* ]]; then
    systemctl enable tasmota-mqtt-update
    systemctl enable api-server-tasmota-update
fi
# Services enable for all images
systemctl enable fruit-server-update
systemctl enable api-server-update
# ======================== End of services enable section ========================

# ======================== Begin of docker images login section ========================
echo $6 | docker --config ~/.broadlink-mqtt login registry.gitlab.com -u gitlab+deploy-token-3642820 --password-stdin
echo $1 | docker --config ~/.api-server login registry.gitlab.com -u gitlab+deploy-token-155513 --password-stdin
echo $4 | docker --config ~/.fruit-server login registry.gitlab.com -u gitlab+deploy-token-3642807 --password-stdin
echo $5 | docker --config ~/.api-server-controllar-ml login registry.gitlab.com -u gitlab+deploy-token-3642447 --password-stdin
echo $7 | docker --config ~/.tasmota-mqtt login registry.gitlab.com -u gitlab+deploy-token-3642722 --password-stdin
echo $8 | docker --config ~/.api-server-tasmota login registry.gitlab.com -u gitlab+deploy-token-3643402 --password-stdin
# ======================== End of docker images login section ========================

# ======================== Begin of cron job section to check for updates every hour ========================
# Images with Multimedia
if [[ $2 == *"multimidia"* ]]; then
    ln -sf /controllar/docker-images/broadlink-mqtt/container-update.sh /etc/cron.hourly/broadlink-mqtt
fi
# Images with ML
if [[ $2 == *"ml"* ]]; then
    ln -sf /controllar/docker-images/api-server-controllar-ml/container-update.sh /etc/cron.hourly/api-server-controllar-ml
fi
# Images with Tasmota
if [[ $2 == *"tasmota"* ]]; then
    ln -sf /controllar/docker-images/tasmota-mqtt/container-update.sh /etc/cron.hourly/tasmota-mqtt
    ln -sf /controllar/docker-images/api-server-tasmota/container-update.sh /etc/cron.hourly/api-server-tasmota
fi
# All images
ln -sf /controllar/docker-images/api-server/container-update.sh /etc/cron.hourly/api-server
ln -sf /controllar/docker-images/fruit-server/container-update.sh /etc/cron.hourly/fruit-server
# ======================== End of cron job section ========================
