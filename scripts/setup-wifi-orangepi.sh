#!/bin/bash

# Clone the driver, install it and configure the wifi
cd /controllar
git clone --depth=1 https://github.com/kelebek333/rtl8188fu
mkdir -p /lib/firmware/rtlwifi/
mkdir -p /etc/modprobe.d/
mkdir -p /etc/NetworkManager/conf.d/

touch /etc/modprobe.d/rtl8188fu.conf
touch /etc/NetworkManager/conf.d/disable-random-mac.conf

echo "options rtl8188fu rtw_power_mgnt=0 rtw_enusbss=0 rtw_ips_mode=0" | tee /etc/modprobe.d/rtl8188fu.conf
echo -e "[device]\nwifi.scan-rand-mac-address=no" | tee /etc/NetworkManager/conf.d/disable-random-mac.conf
echo 'alias usb:v0BDApF179d*dc*dsc*dp*icFFiscFFipFFin* rtl8188fu' | tee /etc/modprobe.d/rtl8xxxu-blacklist.conf