#!/bin/bash
cd /controllar/docker-images/api-server
export DOCKER_CONFIG=~/.api-server
docker-compose pull api-server
docker-compose up -d
docker image prune -f