#!/bin/bash
cd /controllar/docker-images/api-server-controllar-ml
export DOCKER_CONFIG=~/.api-server-controllar-ml
docker-compose pull api-server-controllar-ml
docker-compose up -d
docker image prune -f
