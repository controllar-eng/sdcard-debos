#!/bin/bash
# This script was made for usb-wifi module.
# To setup wifi you need to pass as argument the SSID and PASSWORD.
# Example: ./setup-wifi-orangepi.sh mySSID myPassword
device=$(ifconfig | grep w | awk '{printf $1}' | sed 's/.$//')
echo "CONNECTING to wifi: '$1' password '$2' ifname $device"
nmcli device wifi list | grep :
nmcli device wifi connect "$1" password "$2" ifname $device
echo "DONE"
